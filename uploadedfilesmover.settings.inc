<?php

/**
 * @file
 * This file contains all settings logic.
 */

/**
 * Generates the settings form.
 * @return settings form
 */
function uploadedfilesmover_settingsform() {
  $form = array();
  $form['config']['#tree'] = TRUE;

  $form['markup'] = array(
    '#type'    => 'markup',
    '#value'   => '<p>'. t('<strong>Warning:</strong> Changing the path settings will not take effect on existing nodes.<br />To process the new settings use the batch update on the <em>Maintenance page</em>') .'</p>',
    '#weight'  => -1,
  );

  $form['forbidden_paths'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Excluded paths'),
    '#description' => '<p>'. t('Specifiy multiple paths which are excluded from filesystem cleaning procedure.<br />The paths have to be separated by ;. They are treated as subdirectories of %dir.', array('%dir' => file_directory_path())) .'</p>',
    '#collapsible' => TRUE,
    '#tree'        => TRUE,
  );

  $form['forbidden_paths']['paths'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Paths to exclude'),
    '#size'           => 128,
    '#default_value'  => implode(uploadedfilesmover_getexcludedpaths(), ';'),
    '#description'    => t('Use ; as separator.'),
  );

  // Get all node types
  $nodetypes = node_get_types();

  // Is CCK present?
  $cck = module_exists('content');
  $fields = array();
  if ($cck) {
    $fields = content_fields();
  }

  foreach ($nodetypes as $type) {
    // Filter fields
    $options = array();
    foreach ($fields as $fieldname => $fieldinfo) {
      if ($fieldinfo['type_name'] == $type->type) {
        $options[$fieldname] = check_plain($fieldinfo['widget']['label'] .' ('. $fieldinfo['field_name'] .')');
      }
    } // foreach fields

    // Add attachments if available
    if (variable_get("upload_$type->type", TRUE)) {
      $options['@attachments'] = t('Attachments');
    }

    // Skip this content type if there is no attachment or upload field
    if (!$cck || empty($options)) {
      continue;
    }

    // Get default values
    $default = uploadedfilesmover_getsettings($type->type);

    $form['config'][$type->type]['#tree'] = TRUE;

    $form['config'][$type->type] = array(
      '#type'        => 'fieldset',
      '#title'       => check_plain($type->name),
      '#collapsible' => TRUE,
    );

    $form['config'][$type->type]['enabled'] = array(
      '#type'           => 'checkbox',
      '#title'          => t('Enable upload sorting'),
      '#default_value'  => $default['enabled'],
      '#description'    => t('Enable the module for this content type.'),
    );

    $form['config'][$type->type]['path'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Path'),
      '#size'           => 128,
      '#default_value'  => $default['path'],
      '#description'    => t('The path to move uploaded files. Subdirectory of %dir.<br />Use %type as content type and %title as node title.', array('%dir' => file_directory_path())),
    );

    $form['config'][$type->type]['fields'] = array(
      '#type'          => 'checkboxes',
      '#description'   => t('Select fields to process.'),
      '#options'       => $options,
      '#default_value' => $default['fields'],
    );
  } // foreach node types

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
} // function uploadedfilesmover_settingsform()

/**
 * Validates the settings form.
 * @param form form to validate
 * @param form_state state of the form
 */
function uploadedfilesmover_settingsform_validate($form, &$form_state) {
  // Validate exclude paths
  $excludes = explode(';', trim($form_state['values']['forbidden_paths']['paths'], ' ;'));
  for ($i = 0; $i < count($excludes); $i++) {
    $excludes[$i] = rtrim(trim(check_plain($excludes[$i])), '/\\');
    _uploadedfilesmover_settingsvalidate_path($excludes[$i], 'forbidden_paths][paths', FALSE);
  }
  $form_state['values']['forbidden_paths']['paths'] = implode($excludes, ';');

  // Get node types
  $nodetypes_obj = node_get_types();
  $nodetypes = array();
  foreach ($nodetypes_obj as $type) {
    $nodetypes[] = $type->type;
  }

  foreach ($form_state['values']['config'] as $type => $value) {
    if (!in_array($type, $nodetypes)) {
      // Error: Content type does not exist
      form_set_error('', t('The content type %type does not exist.', array('%type' => check_plain($type))));
      continue;
    }

    // Only validate enabled sections
    if (!$value['enabled']) {
      continue;
    }

    // Check path
    $value['path'] = trim(check_plain($value['path']));
    $form_state['values']['config'][$type]['path'] = $value['path'];
    if (!empty($value['path'])) {
      _uploadedfilesmover_settingsvalidate_path($value['path'], 'config]['. $type .'][fields', TRUE);
    }

    // Filter fields of form
    $form_state['values']['config'][$type]['fields'] = array_filter($value['fields'], '_uploadedfilesmover_settingsvalidate_fieldfilter');

    // Is CCK present?
    $fields = array();
    if (module_exists('content')) {
      $fields = content_fields();
    }

    // Filter CCK fields
    $options = array();
    foreach ($fields as $fieldname => $fieldinfo) {
      if ($fieldinfo['type_name'] == $type) {
        $options[] = $fieldname;
      }
    } // foreach fields

    // Add attachments if available
    if (variable_get("upload_$type", TRUE)) {
      $options[] = '@attachments';
    }

    // At least one field has to be enabled
    if (empty($value['fields'])) {
      form_set_error('config]['. $type .'][fields', t('Please select a field in order to enable functionality on this content type.'));
    }
    else {
      // Check whether there is a field enabled which does not exist
      $valid = TRUE;

      foreach ($form_state['values']['config'][$type]['fields'] as $fieldname => $fieldinfo) {
        if (!in_array($fieldname, $options)) {
            $valid = FALSE;
            break;
        }
      } // foreach fields

      if (!$valid) {
        form_set_error('config]['. $type .'][fields', t('One or more fields do not exist. Please select only existing fields.'));
      }

    } // if empty

  } // foreach node types

} // function uploadedfilesmover_settingsform_validate()

/**
 * Saves the settings of the form.
 * @param form form which is saved
 * @param form_state state of the form
 */
function uploadedfilesmover_settingsform_submit($form, &$form_state) {
  // Check access privileges
  if (!user_access('administer uploaded files mover')) {
    return;
  }

  // Save values
  foreach ($form_state['values']['config'] as $type => $value) {
    variable_set('uploadedfilesmover_'. $type, $value);
  }

  $excludes = explode(';', trim($form_state['values']['forbidden_paths']['paths'], ' ;'));
  for ($i = 0; $i < count($excludes); $i++) {
    $excludes[$i] = rtrim(trim($excludes[$i]), '/\\');
  }
  variable_set('uploadedfilesmover_excludepaths', $excludes);

  drupal_set_message(t('The configuration options have been saved.'));
} // function uploadedfilesmover_settingsform_submit()

/**
 * Helper function to sort out unused fields in the settings form. Used as callback in array_walk().
 * @param elem current element to inspect
 * @return returns the element or NULL to delete it
 */
function _uploadedfilesmover_settingsvalidate_fieldfilter($elem) {
  if ($elem !== 0) {
    return $elem;
  }
  return NULL;
} // function _uploadedfilesmover_settingsvalidate_fieldfilter()

/**
 * Helper function which checks if a given path is valid
 * @param path path to check
 * @param formelement element of the form to set error
 * @param placeholders set to TRUE if placeholders are valid
 * @return TRUE if the path is valid
 */
function _uploadedfilesmover_settingsvalidate_path($path, $formelement, $placeholders) {
  if (empty($path)) {
    return TRUE;
  }

  $valid = TRUE;
  if ($placeholders) {
    if (!preg_match('!^([a-z0-9A-Z]|%type|%title|/|-|_)+$!', $path)) {
      form_set_error($formelement, t('The path must contain only letters, numbers, dashes, slashes, placeholders and underscores.'));
      $valid = FALSE;
    }
  }
  else {
    if (!preg_match('!^([a-z0-9A-Z]|/|-|_)+$!', $path)) {
      form_set_error($formelement, t('The path must contain only letters, numbers, dashes, slashes and underscores.'));
      $valid = FALSE;
    }
  }

  // Check for empty parts in the path
  $parts = explode('/', rtrim($path, ' /\\'));
  foreach ($parts as $pathpart) {
    $pathpart = trim($pathpart);
    if ($pathpart == '') {
      form_set_error($formelement, t('The path must not contain empty directories.'));
      $valid = FALSE;
    }
    if (($pathpart == '..') || ($pathpart == '.')) {
      form_set_error($formelement, t('The path must not be relative.'));
      $valid = FALSE;
    }

  } // foreach

  return $valid;
} // function _uploadedfilesmover_settingsvalidate_path()
