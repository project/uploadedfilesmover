
-- SUMMARY --

The uploadedfilesmover module enables you to specify directories (with placeholders)
for your upload-enabled content types. Using this method you can maintain a clear
and easy directory structure for your uploaded files.

To submit bug reports and feature suggestions, or to track changes:


-- REQUIREMENTS --

Upload or File_Field module.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure the settings in Administer >> Site Configuration >> Uploaded Files Mover.

-- TROUBLESHOOTING --


-- FAQ --


-- CONTACT --

Current maintainers:
* Samuel Leweke (Neodym) - http://drupal.org/user/279608

