<?php

/**
 * @file
 * This file contains all maintenance logic.
 */

/**
 * Implements hook_cron. Starts the cleaning process.
 */
function uploadedfilesmover_cron() {
  // Check access privileges
  if (!user_access('administer uploaded files mover')) {
    return;
  }

  uploadedfilesmover_maintenance_cleandirectories(TRUE);
} // function uploadedfilesmover_cron()

/**
 * Generates the maintenance form
 * @return maintenance form
 */
function uploadedfilesmover_maintenanceform() {
  $form = array();

  $form['clean_directories'] = array(
    '#type' => 'fieldset',
    '#title' => t('Clean empty directories'),
    '#description' => t('This will clean empty directory trees.'),
  );

  $form['clean_directories']['clean'] = array(
    '#type' => 'submit',
    '#value' => t('Clean directories'),
    '#submit' => array('uploadedfilesmover_maintenance_cleandirectories_submit'),
  );

  $form['update_nodes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update paths of nodes'),
    '#description' => t('This will check all nodes and move the files to the path specified by the settings.<br /><strong>Warning:</strong> Depending on the amount of nodes this will take serious time. Consider taking the site offline for this operation.'),
  );

  $form['update_nodes']['update'] = array(
    '#type' => 'submit',
    '#value' => t('Move files'),
    '#submit' => array('uploadedfilesmover_maintenance_movefiles_submit'),
  );
  return $form;
} // function uploadedfilesmover_maintenanceform()

/**
 * Handles the move-files-process initiated by the user in the maintenance form.
 * The batch process is initialized and started.
 */
function uploadedfilesmover_maintenance_movefiles_submit() {
  // Check access privileges
  if (!user_access('administer uploaded files mover')) {
    return;
  }

  // Get node types
  $nodetypes_obj = node_get_types();
  $operations = array();

  // Create operations
  $settings = array();
  foreach ($nodetypes_obj as $type) {
    $settings[$type->type] = uploadedfilesmover_getsettings($type->type);
    if ($settings[$type->type]['enabled']) {
      $operations[] = array('uploadedfilesmover_batchmove', array($type->type, $settings[$type->type]));
    }
  } // foreach

  // Create cleaning operations
  if (!empty($operations)) {
    foreach ($nodetypes_obj as $type) {
      if (!$settings[$type->type]['enabled']) {
        continue;
      }
      $operations[] = array('uploadedfilesmover_batchclean', array($type->type, $settings[$type->type]));
    } // foreach

    $operations[] = array('uploadedfilesmover_batchflushcache', array());
  } // if

  $batch = array(
    'operations'       => $operations,
    'finished'         => 'uploadedfilesmover_batchmove_finished',
    'title'            => t('Moving files'),
    'init_message'     => t('Batch process is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message'    => t('The batch process has encountered an error.'),
    'file'             => drupal_get_path('module', 'uploadedfilesmover') .'/uploadedfilesmover.maintenance.inc',
  );

  batch_set($batch);
} // function uploadedfilesmover_maintenance_movefiles_submit()

/**
 * Batch-Callback. Processes the move file operations in batch mode. The process will check all nodes of the given
 * type and move files if neccessary. To overcome timeouts, the function is called multiple times processing only
 * five items on every call.
 * @param type type of the nodes to check
 * @param settings settings of the given type
 * @param context batch context
 */
function uploadedfilesmover_batchmove($type, $settings, &$context) {
  // Check access privileges
  if (!user_access('administer uploaded files mover')) {
    return;
  }

  // Initialize values
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = db_result(db_query("SELECT COUNT(DISTINCT nid) FROM {node} WHERE type = '%s'", $type));
  }

  // Get the next five nodes
  $result = db_query_range("SELECT nid FROM {node} WHERE nid > %d AND type = '%s' ORDER BY nid ASC", $context['sandbox']['current_node'], $type, 0, 5);
  while ($row = db_fetch_array($result)) {
    $node = node_load($row['nid'], NULL, TRUE);
    $context['sandbox']['progress']++;
    $context['sandbox']['current_node'] = $node->nid;
    $context['message'] = t('Processing: %node.', array('%node' => check_plain($node->title)));

    // Move files
    if (uploadedfilesmover_sortfiles($node, $settings)) {
      $context['results'][] = t('Moved files of node %node', array('%node' => check_plain($node->title)));
    }
    else {
      $context['results'][] = t('Nothing to move in node %node', array('%node' => check_plain($node->title)));
    }
  } // while

  // Update progress
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
} // function uploadedfilesmover_batchmove()

/**
 * Batch-Callback. Cleans the filesystem in batch mode. The directories of the given type will be cleaned.
 * @param type type of the node
 * @param settings settings of the given type
 * @param context batch context
 */
function uploadedfilesmover_batchclean($type, $settings, &$context) {
  // Check access privileges
  if (!user_access('administer uploaded files mover')) {
    return;
  }

  $context['message'] = t('Cleaning: %type.', array('%type' => $type));

  // Only execute once in this implementation, as the whole sites/default/files/ directory gets cleaned.
  // We are not cleaning type-wise here
  if (isset($context['sandbox']['cleaned']) && $context['sandbox']['cleaned']) {
    return;
  }

  if (uploadedfilesmover_maintenance_cleandirectories(TRUE)) {
    $context['results'][] = t('Cleaned files of node type %type', array('%type' => check_pain($type)));
    $context['sandbox']['cleaned'] = TRUE;
  }
  else {
    $context['results'][] = t('Failed to clean files of node type %type', array('%type' => check_plain($type)));
  }
} // function uploadedfilesmover_batchclean()

/**
 * Batch-Callback. Flushes all caches.
 * @param type type of the node
 * @param settings settings of the given type
 * @param context batch context
 */
function uploadedfilesmover_batchflushcache(&$context) {
  // Check access privileges
  if (!user_access('administer uploaded files mover')) {
    return;
  }

  $context['message'] = t('Flushing caches.');

  // TODO: Find correct cache and flush only this one
  drupal_flush_all_caches();

  $context['results'][] = t('Flushed cache.');
} // function uploadedfilesmover_batchflushcache()

/**
 * Batch 'finished' callback
 * @param success TRUE if the batch process was successful
 * @param results results of the process
 * @param operations failed operations
 */
function uploadedfilesmover_batchmove_finished($success, $results, $operations) {
  if ($success) {
    // Here we do something meaningful with the results.
    $message = t('%count items processed:', array('%count' => count($results)));
    $message .= theme('item_list', $results);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %action.', array('%action' => check_plain($error_operation[0])));
    $message .= theme('item_list', $results);
  }
  drupal_set_message($message);
} // function uploadedfilesmover_batchmove_finished()

/**
 * Handles the clean-directories-process initiated by the user in the maintenance form.
 */
function uploadedfilesmover_maintenance_cleandirectories_submit() {
  uploadedfilesmover_maintenance_cleandirectories();
} // function uploadedfilesmover_maintenance_cleandirectories_submit()

/**
 * Cleans the directories.
 * @param return if set to TRUE the function is silent and returns TRUE on success
 * @return if parameter return is set to TRUE, the function returns TRUE if the cleaning process
 *   has been successful.
 */
function uploadedfilesmover_maintenance_cleandirectories($return = FALSE) {
  // Check access privileges
  if (!user_access('administer uploaded files mover')) {
    return;
  }

  // Collect forbidden directories
  $forbidden = array_merge(array('imagecache', 'imagefield_thumbs'), uploadedfilesmover_getexcludedpaths());
  array_walk($forbidden, '_uploadedfilesmover_prependdirectory');

  // Is CCK present?
  if (module_exists('content')) {

    // Filter fields
    foreach (content_fields() as $fieldname => $fieldinfo) {
      $forbidden[] = file_directory_path() .'/'. $fieldinfo['widget']['file_path'];
    }
  }

  if (uploadedfilesmover_cleanpath(file_directory_path(), $forbidden)) {
    if ($return) {
      return TRUE;
    }
    drupal_set_message(t('The directories have been successfully cleaned.'));
  }
} // function uploadedfilesmover_maintenance_cleandirectories()

/**
 * Cleans a path recursively. Forbidden directories will not be cleaned.
 * @param path path to clean as subpath of sites/default/files.
 * @param forbidden array with forbidden directories
 * @param visited array of visited directories
 * @return TRUE if everything went fine
 */
function uploadedfilesmover_cleanpath($path, $forbidden, $visited = array()) {
  // Check access privileges
  if (!user_access('administer uploaded files mover')) {
    return;
  }

  if (strpos($path, file_directory_path()) === FALSE) {
    $workpath = file_directory_path() .'/'. $path;
  }
  else {
    $workpath = $path;
  }

  if (in_array($workpath, $forbidden) || in_array($workpath, $visited)) {
    return TRUE;
  }
  $visited[] = $workpath;

  if (!is_dir($path)) {
    // Check parent directory
    return uploadedfilesmover_cleanpath(_uploadedfilesmover_getparentdirectory($workpath), $forbidden, $visited);
  }

  // Try to delete the directory
  $files = scandir($workpath);
  $hiddenfiles = array_filter($files, '_uploadedfilesmover_cleanpath_hiddenfilefilter');
  $files = array_filter($files, '_uploadedfilesmover_cleanpath_filefilter');
  if (_uploadedfilesmover_deletedirectory($workpath, $files, $hiddenfiles)) {
    // Continue with the parent directory
    return uploadedfilesmover_cleanpath(_uploadedfilesmover_getparentdirectory($workpath), $forbidden, $visited);
  }

  // Clean subfolders
  $success = TRUE;
  foreach ($files as $file) {
    $curfile = $workpath .'/'. $file;
    if (is_dir($curfile) && !in_array($curfile, $forbidden)) {
      $success = uploadedfilesmover_cleanpath($curfile, $forbidden, $visited) && $success;
    }
  }

  // Try to delete the directory
  if ($workpath != file_directory_path()) {
    if (_uploadedfilesmover_deletedirectory($workpath)) {
      // Continue with the parent directory
      $success = uploadedfilesmover_cleanpath(_uploadedfilesmover_getparentdirectory($workpath), $forbidden, $visited) && $success;
    }
  }
  return $success;
} // function uploadedfilesmover_cleanpath()

/**
 * Helper function to delete a directory if it is empty or if there are only hidden files.
 * @param dir directory to delete
 * @param files array of files in the directory excluding hidden files.
 *   The function will determine this value if it is missing.
 * @param hiddenfiles array of hidden files in the directory.
 *   The function will determine this value if it is missing.
 */
function _uploadedfilesmover_deletedirectory($dir, $files = NULL, $hiddenfiles = NULL) {
  // Check access privileges
  if (!user_access('administer uploaded files mover')) {
    return;
  }

  if (!is_dir($dir)) {
    return TRUE;
  }
  if ($files === NULL) {
    $files = scandir($dir);
    $hiddenfiles = array_filter($files, '_uploadedfilesmover_cleanpath_hiddenfilefilter');
    $files = array_filter($files, '_uploadedfilesmover_cleanpath_filefilter');
  }

  // Directory is empty, so delete hidden files and the directory itself
  if (empty($files)) {
    foreach ($hiddenfiles as $hidden) {
      unlink($dir .'/'. $hidden);
    } // foreach

    return rmdir($dir);
  }
  return FALSE;
} // function _uploadedfilesmover_deletedirectory()

/**
 * Helper function to get the parent directory of a given path.
 * @param path path
 * @return parent directory of the directory specified in path
 */
function _uploadedfilesmover_getparentdirectory($path) {
  $pos = strrpos($path, '/');
  if ($pos !== FALSE) {
    return substr($path, 0, $pos);
  }
  return '';
} // function _uploadedfilesmover_getparentdirectory()

/**
 * Helper function to filter the files of a directory. This filter will remove all hidden files and directories.
 * @param elem current element to filter
 * @return the element if it is not hidden or NULL.
 */
function _uploadedfilesmover_cleanpath_filefilter($elem) {
  // Remove hidden files and relative directories
  if (($elem == '.') || ($elem == '.') || (substr($elem, 0, 1) == '.')) {
    return NULL;
  }
  return $elem;
} // function _uploadedfilesmover_cleanpath_filefilter()

/**
 * Helper function to filter the files of a directory. This filter will output all hidden files and directories.
 * @param elem current element to filter
 * @return the element if it is hidden or NULL.
 */
function _uploadedfilesmover_cleanpath_hiddenfilefilter($elem) {
  // Only output hidden files
  if (($elem == '.') || ($elem == '..')) {
    return NULL;
  }
  if (substr($elem, 0, 1) == '.') {
    return $elem;
  }
  return NULL;
} // function _uploadedfilesmover_cleanpath_hiddenfilefilter()

/**
 * Helper function to prepend the sites/default/files/ to the items.
 * @param item current item
 * @param key key
 */
function _uploadedfilesmover_prependdirectory(&$item, $key) {
  $item = file_directory_path() .'/'. $item;
} // function _uploadedfilesmover_prependdirectory()

/**
 * Cleans the directories type-wise.
 * @param type if set only the paths of given type are cleaned
 * @param return if set to TRUE the function is silent and returns TRUE on success
 */
function uploadedfilesmover_maintenance_cleandirectories_typewise($type = NULL, $return = FALSE) {
  // Collect forbidden directories
  $forbidden = array(file_directory_path() .'/imagecache', file_directory_path() .'/imagefield_thumbs');

  // Is CCK present?
  if (module_exists('content')) {

    // Filter fields
    foreach (content_fields() as $fieldname => $fieldinfo) {
      $forbidden[] = file_directory_path() .'/'. $fieldinfo['widget']['file_path'];
    }
  }

  // Get node types
  if ($type === NULL) {
    $nodetypes_obj = node_get_types();
  }
  else {
    $nodetypes_obj = array(new stdClass());
    $nodetypes_obj[0]->type = $type;
  }
  $success = TRUE;

  // Process directories
  foreach ($nodetypes_obj as $type) {
    $settings = uploadedfilesmover_getsettings($type->type);
    $path = rtrim($settings['path'], '/\\');

    if (empty($path)) {
      continue;
    }

    // Do not clean forbidden path
    if (in_array($path, $forbidden)) {
      continue;
    }

    $success = uploadedfilesmover_cleanpath($path, $forbidden) && $success;
  } // foreach

  if ($success) {
    if ($return) {
      return $success;
    }
    drupal_set_message(t('The directories have been successfully cleaned.'));
  }
} // function uploadedfilesmover_maintenance_cleandirectories_typewise()
